import sys
from sqlalchemy import create_engine
from search import search_by_code
from sqlalchemy.orm import sessionmaker
from atemplate import Airport
from vincenty import vincenty

engine = create_engine('mysql://root:pass@127.0.0.1:3306/mydb', echo=True)

def distance(code1, code2, airport, session):
    d1 = search_by_code(code1, airport, session)
    d2 = search_by_code(code2, airport, session)
    if d1 is None or d2 is None:
        return -1
    S = vincenty([float(d1['latitude']), float(d1['longitude'])],
                 [float(d2['latitude']), float(d2['longitude'])])

    #print(d1['latitude'], d1['longitude'])
    #print(d2['latitude'], d2['longitude'])
    return S


if __name__ == "__main__":
    Session_distance = sessionmaker(bind=engine)
    s = Session_distance()
    CODE_1 = sys.argv[1]
    CODE_2 = sys.argv[2]
    code_1 = search_by_code(CODE_1, Airport, s)
    code_2 = search_by_code(CODE_2, Airport, s)
    S = vincenty([float(code_1['latitude']), float(code_1['longitude'])], [float(code_2['latitude']),
                                                                           float(code_2['longitude'])])
    print("distance: ", S)
